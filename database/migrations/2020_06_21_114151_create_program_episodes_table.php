<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_episodes', function (Blueprint $table) {
            $table->id();
            $table->integer('program_id');
            $table->integer('broadcast_time_id');
            $table->integer('episode_no');
            $table->string('title');
            $table->text('description');
            $table->text('thumbnail');
            $table->text('video');
            $table->string('duration')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_episodes');
    }
}

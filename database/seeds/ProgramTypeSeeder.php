<?php

use Illuminate\Database\Seeder;

class ProgramTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Tv show',
            'Series'
        ];
        foreach ($types as $type) {
            \App\ProgramType::create([
                'type'=>$type
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class AdminPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
          0=>[
              'role'=>'Admin',
              'role_description'=>'Can do everything on admin panel'
          ],
            1=>[
                'role'=>'View users',
                'role_description'=>'Can Only view users registered'
            ],
            2=>[
                'role'=>'CRUD on Programs',
                'role_description'=>'Can create,update,delete,view programs'
            ],
            3=>[
                'role'=>'CRUD on Episodes',
                'role_description'=>'Can create,update,delete,view episodes'
            ]
        ];
        foreach ($permissions as $permission) {
            \App\AdminPermission::create([
                'role'=>$permission['role'],
                'role_description'=>$permission['role_description']
            ]);
        }

    }
}

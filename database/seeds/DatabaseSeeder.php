<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(AdminPermissionSeeder::class);
        $this->call(ProgramTypeSeeder::class);
        $this->call(AdminUserSeeder::class);
    }
}

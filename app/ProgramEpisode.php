<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramEpisode extends Model
{
    protected $table = 'program_episodes';
    protected $fillable = [
        'program_id',
        'broadcast_time_id',
        'broadcast_date',
        'title',
        'description',
        'thumbnail',
        'video'];

    public function program(){
        return $this->belongsTo(
            Program::class,
            'program_id',
            'id');
    }
    public function broadcastTime(){
        return $this->belongsTo(
            BroadcastTime::class,
            'broadcast_time_id',
            'id'
        );
    }
    public function userFavouriteEpisode(){
        return $this->hasMany(
            UserFavouriteEdpisode::class,
            'episode_id',
            'id'
        );
    }
}

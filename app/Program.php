<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'programs';
    protected $fillable =[
        'title',
        'description',
        'starting_date',
        'ending_date'
    ];

    public function broadcastTimes(){
        return $this->hasMany(
          BroadcastTime::class,
          'program_id',
          'id'
        );
    }
    public function programEpisodes(){
        return $this->hasMany(
            ProgramEpisode::class,
            'program_id',
            'id'
        );
    }
    public function userFollowPrograms(){
        return $this->hasMany(
            UserFollowProgram::class,
            'program_id',
            'id'
        );
    }
}

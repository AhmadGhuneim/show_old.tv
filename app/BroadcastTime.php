<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadcastTime extends Model
{
    protected $table = 'broadcast_times';
    protected $fillable = ['program_id','broadcast_day','broadcast_time'];

    public function program(){
        return $this->belongsTo(
            Program::class,
            'program_id',
            'id'
        );
    }
    public function episode(){
        return $this->hasMany(
            ProgramEpisode::class,
            'broadcast_time_id',
            'id'
        );
    }
}

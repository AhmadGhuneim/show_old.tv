<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminPermission extends Model
{
    protected $table = 'admin_permissions';
    protected $fillable = [
        'role',
        'role_description'
    ];
    public function adminUser(){
        return $this->hasMany(
            AdminUser::class,
            'permission_id',
            'id'
        );
    }
}

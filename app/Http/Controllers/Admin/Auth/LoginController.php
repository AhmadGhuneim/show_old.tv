<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm(){
        return view('admin.auth.login');
    }
    public function login(Request $request){
        //Validate form data
        $this->validate($request,[
            'email' =>['required','email'],
            'password'=>['required','min:6'],
        ]);
        //Attempt to login
        $credentials = [
            'email'=>$request->email,
            'password'=>$request->password
        ];

        if(Auth::guard('admin')->attempt($credentials,$request->remember)){
            //If successful, then redirect to their location
            return redirect()->intended(route('admin.dashboard'));
        }

        //If unsuccessful,then redirect back to the login form with errors and data
        return redirect()
            ->back()
            ->withInput($request->only('email','remember'));
    }
    public function logout()
    {
        Auth::guard('admin')->logout();
        return  redirect('/admin');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }
}

<?php
return [
    'route' => [

        'prefix' => env('ADMIN_ROUTE_PREFIX', 'admin'),

        'namespace' => 'Admin',

        'middleware' => ['web', 'auth:admin'],
    ],
];

<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware('optimizeImages')->group(function () {
    // all images will be optimized automatically
    Route::post('upload-images', 'UploadController@index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->resource('/','AdminUserController')
    ->name('index','admin.dashboard');
});
Route::group([
   'prefix'=>config('admin.route.prefix'),
   'namespace'  =>config('admin.route.namespace'),
],function (Router $router){
    $router->get('login',
        'Auth\LoginController@showLoginForm')
        ->name('admin.login');
    $router->post('login',
        'Auth\LoginController@login')
        ->name('admin.login');
    $router->post('logout', 'Auth\LoginController@logout')->name('admin.logout');

});

